class Article < ActiveRecord::Base
	belongs_to :user
	has_many :comments , as: :commentable
	attr_accessible :body, :published_at, :title
	validates :body, :presence => true
	validates :tile , :presence => true


	#this method is made to format the title
   def long_title
    	"#{title} - #{published_at}"
    	
    end
    
end
