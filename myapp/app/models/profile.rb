class Profile < ActiveRecord::Base
	belongs_to :user
  	attr_accessible :birthday, :name, :user_id
end
