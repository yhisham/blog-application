class Post < ActiveRecord::Base
	has_many :comments , as: :commentable
	belongs_to :user
  	attr_accessible :content, :title
  	validates :content, :presence => true
	validates :title , :presence => true

end
