class User < ActiveRecord::Base
	#relations
	has_one :profile
	has_many :articles
	has_many :posts

	#to avoid mass assignment
 	attr_accessible :email, :password
 	 attr_accessor :password



 	#validations
 	validates_confirmation_of :password
  	validates_presence_of :password, :on => :create
  	validates_presence_of :email
  	validates_uniqueness_of :email


	#variables
	attr_accessor :password


	#methods
	def self.authenticate(email, password)
		#find user by email
    	user = find_by_email(email)
    return user if user && user.authenticated?(password)
  end

  def authenticated?(password)
    password
  end

	
end
