class ApplicationController < ActionController::Base
  protect_from_forgery
  force_ssl

  	#sets current_user available for view
 	helper_method :current_user

	private

	#this method sets the current_user with the user of the session , used in app.html.erb
	def current_user
  		@current_user ||= User.find(session[:user_id]) if session[:user_id]
	end


end
